const express = require('express');
const app = express();
const port = 3000;

const randomNumberBetween = (min, max) => {
  return Math.round(Math.random() * (max - min) + min);
};

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*'); // update to match the domain you will make the request from
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

app.post('/KpiTest', (req, res) => {
  res.send({
    ErrorCode: 0,
    KpiId: 'some-id',
    MessagesSent: randomNumberBetween(10, 100000),
    CustomerReached: randomNumberBetween(10, 10000),
    CustomerOptedIn: randomNumberBetween(10, 1000),
    CustomerOptedOut: randomNumberBetween(10, 1000),
  });
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});

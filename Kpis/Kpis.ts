interface KpiItem {
  id: string;
  name?: string;
  number?: number;
  color?: string;
  icon?: string;
}

interface KpiUpdateEventData {
  kpiId: string;
  kpiListToUpdate: KpiItem[];
}

class KpiCards {
  private kpiCardContainer: JQuery<HTMLElement>;

  constructor(containerId: string, kpiList: KpiItem[]) {
    const self = this;
    self.kpiCardContainer = $(containerId);
    self.kpiCardContainer.hasClass('kpi-card-container') ? null : self.kpiCardContainer.addClass('kpi-card-container');
    self.GenerateKpis(kpiList);
    self.kpiCardContainer.on('kpiUpdate', function (event, kpiUpdateEventData: KpiUpdateEventData) {
      if (kpiUpdateEventData.kpiId === containerId) {
        self.Update(kpiUpdateEventData.kpiListToUpdate);
      }
    });
  }

  public GetContainer(): JQuery<HTMLElement> {
    return this.kpiCardContainer;
  }

  private GenerateKpis(kpiList: KpiItem[]): void {
    if (this.IsValidKpiList(kpiList)) {
      kpiList.forEach((kpiItem) => {
        this.GetContainer().append(
          `<div class="kpi-card kpi-${kpiItem.id}">
            <h2 class="kpi-card-title">${kpiItem.name ? kpiItem.name : ''}</h2>
            <p class="kpi-card-number">${kpiItem.number ? this.NumberWithCommas(kpiItem.number) : '-'}</p>
            <div class="kpi-card-icon" style="background-color: ${kpiItem.color ? kpiItem.color : 'transparent'}">
              ${kpiItem.icon ? '<img src="' + kpiItem.icon + '" alt="Icon" />' : ''}
            </div>
          </div>`
        );
      });
    }
  }

  private IsValidKpiList(kpiListToTest: KpiItem[]): boolean {
    if (Object.prototype.toString.call(kpiListToTest) !== '[object Array]') {
      console.error(kpiListToTest);
      throw new Error('You must provide an array of KpiItem objects as an Update argument.');
    }
    kpiListToTest.forEach((kpiItem) => {
      if (!kpiItem.hasOwnProperty('id')) {
        console.error(kpiItem);
        throw new Error('KpiItem does not have valid properties.');
      }
    });
    return true;
  }

  private NumberWithCommas(x: string | number) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  }

  private Update(kpiListToUpdate: KpiItem[]) {
    if (this.IsValidKpiList(kpiListToUpdate)) {
      kpiListToUpdate.forEach((kpiItem) => {
        if (kpiItem.name) {
          this.kpiCardContainer.find(`.kpi-${kpiItem.id} .kpi-card-title`).text(kpiItem.name);
        }
        if (kpiItem.number) {
          this.kpiCardContainer.find(`.kpi-${kpiItem.id} .kpi-card-number`).text(this.NumberWithCommas(kpiItem.number));
        }
        if (kpiItem.color) {
          this.kpiCardContainer.find(`.kpi-${kpiItem.id} .kpi-card-icon`).css('background-color', kpiItem.color);
        }
      });
    }
  }
}

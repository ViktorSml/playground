// interface KpiCardsResponseData {
//   ErrorCode: number;
//   KpiId: string;
//   MessagesSent: number;
//   CustomerReached: number;
//   CustomerOptedIn: number;
//   CustomerOptedOut: number;
// }

function postRequest(apiUrl, data, callback) {
  return $.ajax({
    type: 'POST',
    url: apiUrl,
    data: data,
    dataType: 'json',
    success: function (apiResponse, status) {
      if (callback) {
        callback(apiResponse, status);
      }
    },
  });
}

$(document).ready(function () {
  var kpiContainerId = '#digital-engagement-kpis';
  var demoRequestData = { kpiId: 'current-month', clientId: 9, from: '2020-08-01', to: '2020-09-01' };
  postRequest('http://localhost:3000/KpiTest', demoRequestData, function (apiResponse) {
    new KpiCards(kpiContainerId, [
      {
        id: 'messages-sent',
        name: 'Total messages sent',
        number: apiResponse.MessagesSent,
        color: 'rgba(84, 18, 223, 0.07)',
        icon: 'svg/icon.svg',
      },
      {
        id: 'costumers-reached',
        name: 'Costumers reached',
        number: apiResponse.CustomerReached,
        color: 'rgba(3, 156, 222, 0.1)',
        icon: 'svg/icon.svg',
      },
      {
        id: 'costumers-opted-in',
        name: 'Opted-in users',
        number: apiResponse.CustomerOptedIn,
        color: 'rgba(2, 215, 100, 0.1)',
        icon: 'svg/icon.svg',
      },
      {
        id: 'costumers-opted-out',
        name: 'Opted-out users',
        number: apiResponse.CustomerOptedOut,
        color: 'rgba(255, 0, 0, 0.1)',
        icon: 'svg/icon.svg',
      },
    ]);
  });

  $('button').click(function () {
    postRequest('http://localhost:3000/KpiTest', demoRequestData, function (apiResponse, status) {
      // start update kpi only code
      $(kpiContainerId).trigger('kpiUpdate', {
        kpiId: kpiContainerId,
        kpiListToUpdate: [
          { id: 'messages-sent', number: apiResponse.MessagesSent, name: apiResponse.MessagesSent },
          { id: 'costumers-reached', number: apiResponse.CustomerReached, color: 'grey' },
          { id: 'costumers-opted-in', number: apiResponse.CustomerOptedIn },
          { id: 'costumers-opted-out', number: apiResponse.CustomerOptedOut },
          { id: 'dsadasd', number: apiResponse.CustomerOptedOut },
        ],
      });
      // ends update kpi only code
    });
  });
});

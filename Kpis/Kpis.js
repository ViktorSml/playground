var KpiCards = /** @class */ (function () {
    function KpiCards(containerId, kpiList) {
        var self = this;
        self.kpiCardContainer = $(containerId);
        self.kpiCardContainer.hasClass('kpi-card-container') ? null : self.kpiCardContainer.addClass('kpi-card-container');
        self.GenerateKpis(kpiList);
        self.kpiCardContainer.on('kpiUpdate', function (event, kpiUpdateEventData) {
            if (kpiUpdateEventData.kpiId === containerId) {
                self.Update(kpiUpdateEventData.kpiListToUpdate);
            }
        });
    }
    KpiCards.prototype.GetContainer = function () {
        return this.kpiCardContainer;
    };
    KpiCards.prototype.GenerateKpis = function (kpiList) {
        var _this = this;
        if (this.IsValidKpiList(kpiList)) {
            kpiList.forEach(function (kpiItem) {
                _this.GetContainer().append("<div class=\"kpi-card kpi-" + kpiItem.id + "\">\n            <h2 class=\"kpi-card-title\">" + (kpiItem.name ? kpiItem.name : '') + "</h2>\n            <p class=\"kpi-card-number\">" + (kpiItem.number ? _this.NumberWithCommas(kpiItem.number) : '-') + "</p>\n            <div class=\"kpi-card-icon\" style=\"background-color: " + (kpiItem.color ? kpiItem.color : 'transparent') + "\">\n              " + (kpiItem.icon ? '<img src="' + kpiItem.icon + '" alt="Icon" />' : '') + "\n            </div>\n          </div>");
            });
        }
    };
    KpiCards.prototype.IsValidKpiList = function (kpiListToTest) {
        if (Object.prototype.toString.call(kpiListToTest) !== '[object Array]') {
            console.error(kpiListToTest);
            throw new Error('You must provide an array of KpiItem objects as an Update argument.');
        }
        kpiListToTest.forEach(function (kpiItem) {
            if (!kpiItem.hasOwnProperty('id')) {
                console.error(kpiItem);
                throw new Error('KpiItem does not have valid properties.');
            }
        });
        return true;
    };
    KpiCards.prototype.NumberWithCommas = function (x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    };
    KpiCards.prototype.Update = function (kpiListToUpdate) {
        var _this = this;
        if (this.IsValidKpiList(kpiListToUpdate)) {
            kpiListToUpdate.forEach(function (kpiItem) {
                if (kpiItem.name) {
                    _this.kpiCardContainer.find(".kpi-" + kpiItem.id + " .kpi-card-title").text(kpiItem.name);
                }
                if (kpiItem.number) {
                    _this.kpiCardContainer.find(".kpi-" + kpiItem.id + " .kpi-card-number").text(_this.NumberWithCommas(kpiItem.number));
                }
                if (kpiItem.color) {
                    _this.kpiCardContainer.find(".kpi-" + kpiItem.id + " .kpi-card-icon").css('background-color', kpiItem.color);
                }
            });
        }
    };
    return KpiCards;
}());

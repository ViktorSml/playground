interface PillTabsSettings {
  initialTab?: number;
}

class PillTabs {
  private pillTabsContainer: JQuery<HTMLElement>;
  private flexBackground: JQuery<HTMLElement>;
  private settings: PillTabsSettings;

  constructor(containerId: string, pillTabsSettings?: PillTabsSettings) {
    const defaultSettings: PillTabsSettings = { initialTab: 0 };
    this.pillTabsContainer = $(containerId);
    this.settings = { ...defaultSettings, ...pillTabsSettings };
    this.InitializeEachTab();
    this.InitializeFlexBackground();
    const initialTab = this.pillTabsContainer.find(`li:nth-child(${this.settings.initialTab + 1})`);
    this.flexBackground.css({ width: initialTab.outerWidth(), left: initialTab[0].offsetLeft });
    this.pillTabsContainer.addClass('initalized');
  }

  private InitializeEachTab() {
    this.pillTabsContainer.find('li').each((index, pillTab) => {
      if (index === this.settings.initialTab) {
        $(pillTab).addClass('active');
      }
      $(pillTab).click((event) => {
        const clickedTab = event.target;
        this.pillTabsContainer.find('.active').removeClass('active');
        this.pillTabsContainer.trigger('pillTabChange', $(clickedTab).data());
        this.flexBackground.css({ width: $(pillTab).outerWidth(), left: pillTab.offsetLeft });
        $(clickedTab).addClass('active');
      });
    });
  }

  private InitializeFlexBackground() {
    this.pillTabsContainer.append('<div class="flexible-background"></div>');
    this.flexBackground = this.pillTabsContainer.find('.flexible-background');
  }

  public getContainer(): JQuery<HTMLElement> {
    return this.pillTabsContainer;
  }
}

$(document).ready(() => {
  const pillTabs = new PillTabs('#demo-pill-tabs', {
    initialTab: 2,
  });

  pillTabs.getContainer().on('pillTabChange', function (event, dataAttributes) {
    console.log('From event listener! 🧙‍♂️', dataAttributes);
  });
});

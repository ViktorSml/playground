var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var PillTabs = /** @class */ (function () {
    function PillTabs(containerId, pillTabsSettings) {
        var defaultSettings = { initialTab: 0 };
        this.pillTabsContainer = $(containerId);
        this.settings = __assign(__assign({}, defaultSettings), pillTabsSettings);
        this.InitializeEachTab();
        this.InitializeFlexBackground();
        var initialTab = this.pillTabsContainer.find("li:nth-child(" + (this.settings.initialTab + 1) + ")");
        this.flexBackground.css({ width: initialTab.outerWidth(), left: initialTab[0].offsetLeft });
        this.pillTabsContainer.addClass('initalized');
    }
    PillTabs.prototype.InitializeEachTab = function () {
        var _this = this;
        this.pillTabsContainer.find('li').each(function (index, pillTab) {
            if (index === _this.settings.initialTab) {
                $(pillTab).addClass('active');
            }
            $(pillTab).click(function (event) {
                var clickedTab = event.target;
                _this.pillTabsContainer.find('.active').removeClass('active');
                _this.pillTabsContainer.trigger('pillTabChange', $(clickedTab).data());
                _this.flexBackground.css({ width: $(pillTab).outerWidth(), left: pillTab.offsetLeft });
                $(clickedTab).addClass('active');
            });
        });
    };
    PillTabs.prototype.InitializeFlexBackground = function () {
        this.pillTabsContainer.append('<div class="flexible-background"></div>');
        this.flexBackground = this.pillTabsContainer.find('.flexible-background');
    };
    PillTabs.prototype.getContainer = function () {
        return this.pillTabsContainer;
    };
    return PillTabs;
}());
$(document).ready(function () {
    var pillTabs = new PillTabs('#demo-pill-tabs', {
        initialTab: 2
    });
    pillTabs.getContainer().on('pillTabChange', function (event, dataAttributes) {
        console.log('From event listener! 🧙‍♂️', dataAttributes);
    });
});
